package com.qfjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chapter6SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chapter6SpringbootApplication.class, args);
    }

}
