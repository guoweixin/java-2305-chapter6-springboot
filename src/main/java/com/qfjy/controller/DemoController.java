package com.qfjy.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController {


    @GetMapping("info")
    @ResponseBody
    public String info(){

        return  "Hello Docker SpringBoot 3.0.1";
    }
    // Hello java 2305 1
    //Hello java 2305 2
    //Hello java 2305 2024
    // 我是开发人员新增的一行代码： Hello World
    //23 行 -dev 我是开发人员
    // 24行，我是项目经理
    //原神到此一游
    //json 到此一游
    //order 到此一游
    //我是sb
    //高东升最帅 王博峰是狗
    //狗高东升
    //急急急
    //郭宇杰
    //陈景珠
    //安起航
    //nkx
    //我汤姆莱纳
    //aaa
    //朱雯倩
    //徐梦迪
    //嘻嘻
    //你好
}
